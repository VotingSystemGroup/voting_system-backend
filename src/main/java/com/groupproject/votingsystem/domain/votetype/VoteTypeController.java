package com.groupproject.votingsystem.domain.votetype;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/votetype")
public class VoteTypeController {

    private final VoteTypeRepository voteTypeRepository;

    private VoteTypeController(VoteTypeRepository voteTypeRepository) {
        this.voteTypeRepository = voteTypeRepository;
    }

    @GetMapping("name/{name}")
    List<VoteType> getAllByName(@PathVariable String name) {
        return voteTypeRepository.findAllByNameContainingIgnoreCase(name);
    }

    @GetMapping("region/{region}")
    List<VoteType> getAllByRegion(@PathVariable String region) {
        return voteTypeRepository.findAllByRegion_VoivodeshipContainingIgnoreCaseOrRegionCountContainingIgnoreCaseOrRegion_CommuneContainingIgnoreCase(region, region, region);
    }

    @GetMapping("id/{id}")
    public VoteType getById(@PathVariable Long id) {
        return voteTypeRepository.findById(id).orElse(null);
    }

    @GetMapping
    public List<VoteType> getAll() {
        return voteTypeRepository.findAll();
    }
}
