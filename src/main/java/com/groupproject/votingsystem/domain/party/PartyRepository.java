package com.groupproject.votingsystem.domain.party;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PartyRepository extends JpaRepository<Party, Long> {

    List<Party> findAllByNameContainingIgnoreCase(String name);
}
