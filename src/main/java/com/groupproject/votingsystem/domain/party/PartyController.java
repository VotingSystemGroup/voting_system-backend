package com.groupproject.votingsystem.domain.party;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/party")
public class PartyController {

    private final PartyRepository partyRepository;

    private PartyController(PartyRepository partyRepository) {
        this.partyRepository = partyRepository;
    }

    @GetMapping("name/{name}")
    List<Party> getAllPartyByName(@PathVariable String name) {
        return partyRepository.findAllByNameContainingIgnoreCase(name);
    }

    @GetMapping("id/{id}")
    public Party getById(@PathVariable Long id) {
        return partyRepository.findById(id).orElse(null);
    }

    @GetMapping
    public List<Party> getAll() {
        return partyRepository.findAll();
    }
}
