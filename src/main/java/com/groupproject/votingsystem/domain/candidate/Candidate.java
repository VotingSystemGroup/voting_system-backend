package com.groupproject.votingsystem.domain.candidate;

import com.groupproject.votingsystem.domain.party.Party;
import com.groupproject.votingsystem.domain.person.Person;
import com.groupproject.votingsystem.domain.votetype.VoteType;

import javax.persistence.*;

@Entity
@Table(schema = "PUBLIC", name = "CANDIDATE")
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VOTE_TYPE_ID")
    private VoteType voteType;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID")
    private Person person;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARTY_ID")
    private Party party;

    @Column(name = "LIST_POSITION")
    private Long listPosition;

    public Candidate() {
    }

    public Long getId() {
        return id;
    }

    public VoteType getVoteType() {
        return voteType;
    }

    public Person getPerson() {
        return person;
    }

    public Party getParty() {
        return party;
    }

    public Long getListPosition() {
        return listPosition;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setVoteType(VoteType voteType) {
        this.voteType = voteType;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public void setListPosition(Long listPosition) {
        this.listPosition = listPosition;
    }
}
