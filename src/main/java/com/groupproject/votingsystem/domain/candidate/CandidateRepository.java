package com.groupproject.votingsystem.domain.candidate;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    List<Candidate> findAllByVoteType_NameContainingIgnoreCase(String name);

    List<Candidate> findAllByVoteType_Region_VoivodeshipContainingIgnoreCaseOrVoteType_RegionCountContainingIgnoreCaseOrVoteType_Region_CommuneContainingIgnoreCase(String voivodeship, String count, String commune);

    List<Candidate> findAllByPerson_FirstNameContainingIgnoreCaseOrPerson_LastNameContainingIgnoreCase(String firstName, String lastName);

    List<Candidate> findAllByPerson_PeselEquals(Long pesel);

    List<Candidate> findAllByPerson_IdCardNumberEqualsIgnoreCase(String idCardNumber);

    List<Candidate> findAllByPerson_Region_VoivodeshipContainingIgnoreCaseOrPerson_RegionCountContainingIgnoreCaseOrPerson_Region_CommuneContainingIgnoreCase(String voivodeship, String count, String commune);

    List<Candidate> findAllByPerson_CityContainingIgnoreCase(String city);

    List<Candidate> findAllByParty_NameContainingIgnoreCase(String party);

    List<Candidate> findAllByListPositionEquals(Long listPosition);
}
