package com.groupproject.votingsystem.domain.vote;

import com.groupproject.votingsystem.domain.person.Person;
import com.groupproject.votingsystem.domain.votetype.VoteType;

import javax.persistence.*;

@Entity
@Table(schema = "PUBLIC", name = "VOTE")
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERSON_ID")
    private Person person;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "VOTE_TYPE_ID")
    private VoteType voteType;

    public Vote() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public VoteType getVoteType() {
        return voteType;
    }

    public void setVoteType(VoteType voteType) {
        this.voteType = voteType;
    }
}
