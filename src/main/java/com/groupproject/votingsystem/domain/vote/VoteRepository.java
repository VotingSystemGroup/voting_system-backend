package com.groupproject.votingsystem.domain.vote;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VoteRepository extends JpaRepository<Vote, Long> {

    List<Vote> findAllByPerson_FirstNameContainingIgnoreCaseOrPerson_LastNameContainingIgnoreCase(String firstName, String lastName);

    List<Vote> findAllByPerson_PeselEquals(Long pesel);

    List<Vote> findAllByPerson_IdCardNumberEqualsIgnoreCase(String idCardNumber);

    List<Vote> findAllByPerson_Region_VoivodeshipContainingIgnoreCaseOrPerson_RegionCountContainingIgnoreCaseOrPerson_Region_CommuneContainingIgnoreCase(String voivodeship, String count, String commune);

    List<Vote> findAllByPerson_CityContainingIgnoreCase(String city);

    List<Vote> findAllByVoteType_NameContainingIgnoreCase(String name);

    List<Vote> findAllByVoteType_Region_VoivodeshipContainingIgnoreCaseOrVoteType_RegionCountContainingIgnoreCaseOrVoteType_Region_CommuneContainingIgnoreCase(String voivodeship, String count, String commune);
}
