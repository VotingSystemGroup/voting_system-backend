package com.groupproject.votingsystem.domain.vote;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/vote")
public class VoteController {

    private final VoteRepository voteRepository;

    private VoteController(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    @GetMapping("person/name/{personName}")
    List<Vote> getAllByPersonName(@PathVariable String personName) {
        return voteRepository.findAllByPerson_FirstNameContainingIgnoreCaseOrPerson_LastNameContainingIgnoreCase(personName, personName);
    }

    @GetMapping("person/pesel/{personPesel}")
    List<Vote> getAllByPersonPesel(@PathVariable Long personPesel) {
        return voteRepository.findAllByPerson_PeselEquals(personPesel);
    }

    @GetMapping("person/idCardNumber/{personIdCardNumber}")
    List<Vote> getAllByPersonIdCardNumber(@PathVariable String personIdCardNumber) {
        return voteRepository.findAllByPerson_IdCardNumberEqualsIgnoreCase(personIdCardNumber);
    }

    @GetMapping("person/region/{personRegion}")
    List<Vote> getAllByPersonRegion(@PathVariable String personRegion) {
        return voteRepository.findAllByPerson_Region_VoivodeshipContainingIgnoreCaseOrPerson_RegionCountContainingIgnoreCaseOrPerson_Region_CommuneContainingIgnoreCase(personRegion, personRegion, personRegion);
    }

    @GetMapping("person/city/{personCity}")
    List<Vote> getAllByPersonCity(@PathVariable String personCity) {
        return voteRepository.findAllByPerson_CityContainingIgnoreCase(personCity);
    }

    @GetMapping("voteType/name/{voteTypeName}")
    List<Vote> getAllByVoteTypeName(@PathVariable String voteTypeName) {
        return voteRepository.findAllByVoteType_NameContainingIgnoreCase(voteTypeName);
    }

    @GetMapping("voteType/region/{voteTypeRegion}")
    List<Vote> getAllByVoteTypeRegion(@PathVariable String voteTypeRegion) {
        return voteRepository.findAllByVoteType_Region_VoivodeshipContainingIgnoreCaseOrVoteType_RegionCountContainingIgnoreCaseOrVoteType_Region_CommuneContainingIgnoreCase(voteTypeRegion, voteTypeRegion, voteTypeRegion);
    }

    @GetMapping("id/{id}")
    public Vote getById(@PathVariable Long id) {
        return voteRepository.findById(id).orElse(null);
    }

    @GetMapping
    public List<Vote> getAll() {
        return voteRepository.findAll();
    }

    @PostMapping
    public Vote add(@Valid @RequestBody Vote vote) {
        System.out.println(vote);
        return voteRepository.saveAndFlush(vote);
    }

    @PutMapping("{id}")
    public ResponseEntity edit(@PathVariable Long id, @Valid @RequestBody Vote vote) {
        if (!id.equals(vote.getId())) {
            return ResponseEntity.badRequest().build();
        }
        Optional<Vote> existingVote = voteRepository.findById(id);
        if (existingVote.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        voteRepository.save(vote);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        Optional<Vote> existingVote = voteRepository.findById(id);
        if (existingVote.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        voteRepository.deleteById(id);

        return ResponseEntity.ok().build();
    }
}
