package com.groupproject.votingsystem.domain.person;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    private final PersonRepository personRepository;

    private PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping("name/{name}")
    List<Person> getAllPeopleByName(@PathVariable String name) {
        return personRepository.findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(name, name);
    }

    @GetMapping("pesel/{pesel}")
    List<Person> getAllPeopleByPesel(@PathVariable Long pesel) {
        return personRepository.findAllByPeselEquals(pesel);
    }

    @GetMapping("idCardNumber/{idCardNumber}")
    List<Person> getAllPeopleByIdCardNumber(@PathVariable String idCardNumber) {
        return personRepository.findAllByIdCardNumberEqualsIgnoreCase(idCardNumber);
    }

    @GetMapping("region/{region}")
    List<Person> getAllPeopleByRegion(@PathVariable String region) {
        return personRepository.findAllByRegion_VoivodeshipContainingIgnoreCaseOrRegionCountContainingIgnoreCaseOrRegion_CommuneContainingIgnoreCase(region, region, region);
    }

    @GetMapping("city/{city}")
    List<Person> getAllByCityContainingIgnoreCase(@PathVariable String city) {
        return personRepository.findAllByCityContainingIgnoreCase(city);
    }

    @GetMapping("id/{id}")
    public Person getById(@PathVariable Long id) {
        return personRepository.findById(id).orElse(null);
    }

    @GetMapping
    public List<Person> getAll() {
        return personRepository.findAll();
    }
}
