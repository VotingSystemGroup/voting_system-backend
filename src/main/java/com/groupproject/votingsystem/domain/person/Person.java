package com.groupproject.votingsystem.domain.person;

import com.groupproject.votingsystem.domain.region.Region;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(schema = "PUBLIC", name = "PERSON")
public class Person {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "PESEL_NUMBER")
    private Long pesel;

    @Column(name = "ID_CARD_NUMBER")
    private String idCardNumber;

    @Column(name = "ID_CARD_EXP_DATE")
    private LocalDate idCardExpirationDate;

    @Column(name = "LAST_INCOME_VALUE")
    private Long lastIncomeValue;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "REGION_ID")
    private Region region;

    @Column(name = "CITY")
    private String city;

    public Person() {
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getPesel() {
        return pesel;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public LocalDate getIdCardExpirationDate() {
        return idCardExpirationDate;
    }

    public Long getLastIncomeValue() {
        return lastIncomeValue;
    }

    public Region getRegion() {
        return region;
    }

    public String getCity() {
        return city;
    }
}
