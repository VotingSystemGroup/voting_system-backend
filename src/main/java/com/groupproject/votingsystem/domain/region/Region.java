package com.groupproject.votingsystem.domain.region;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "PUBLIC", name = "REGION")
public class Region {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "VOIVODESHIP_CODE")
    private Long voivodeshipCode;

    @Column(name = "COUNTY_CODE")
    private Long countCode;

    @Column(name = "VOIVODESHIP")
    private String voivodeship;

    @Column(name = "COUNTY")
    private String count;

    @Column(name = "COMMUNE")
    private String commune;

    public Region() {
    }

    public Long getId() {
        return id;
    }

    public Long getVoivodeshipCode() {
        return voivodeshipCode;
    }

    public Long getCountCode() {
        return countCode;
    }

    public String getVoivodeship() {
        return voivodeship;
    }

    public String getCount() {
        return count;
    }

    public String getCommune() {
        return commune;
    }
}
